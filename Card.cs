﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Poker
{
    class Card
    {
        public enum SUIT
        {
            HEARTS,
            SPADES,
            DIAMONDS,
            CLUBS
        }

        public enum CARDVALUE // Number on the card
        {
            TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT,
            NINE, TEN, JACK, QUEEN, KING, ACE
        }

        // PROPERTIES FOR RETURNED VALUES
        public SUIT MySuit { get; set; }
        public CARDVALUE MyValue { get; set; }
    }
}
