﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Poker
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(85, 40);
            // remove scroll bars by setting buffer height & width to actual height & width
            Console.BufferWidth = 85;
            Console.BufferHeight = 40;
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.Title = "Poker Game";

            DealCards dc = new DealCards();
            bool quit = false;

            while (!quit)
            {
                dc.Deal();

                char selection = ' ';
                while (!selection.Equals('Y') && !selection.Equals('N'))
                {
                    Console.WriteLine("Play again? Y/N");
                    try
                    {
                        selection = Convert.ToChar(Console.ReadLine().ToUpper());
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                 }

                if (selection.Equals('Y')) 
                {
                    quit = false;
                }
                else if (selection.Equals('N'))
                {
                    quit = true;
                }
                else
                {
                    Console.WriteLine("Invalid input");
                }
            }
        }
    }
}
