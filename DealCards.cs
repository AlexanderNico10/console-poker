﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Poker
{
    class DealCards: DeckOfCards
    {
        private Card[] playerHand;
        private Card[] computerHand;
        private Card[] sortedPlayerHand;
        private Card[] sortedComputerHand;

        public DealCards()
        {
            playerHand = new Card[5];
            sortedPlayerHand = new Card[5];
            computerHand = new Card[5];
            sortedComputerHand = new Card[5];
        }

        public void Deal()
        {
            setUpDeck(); // creates deck of cards and shuffles them
            getHand();
            sortCards();
            displayCards();
            evaluateHand();
        }

        public void getHand()
        {
            for (int i = 0; i < 5; i++) // run the loop 5 times coz 5 cards are dealt
            {
                playerHand[i] = getDeck[i]; // 5 cards for the player
            }

            for (int i = 5; i < 10; i++)
            {
                computerHand[i - 5] = getDeck[i]; // 5 cards for the pc | computerHand index 0 = 5th card from deck getDeck
            }
        }

        public void sortCards()
        {
            var queryPlayer = from hand in playerHand
                              orderby hand.MyValue
                              select hand;

            var queryComputer = from hand in computerHand
                                orderby hand.MyValue
                                select hand;

            // Now to populate the sorted arrays with card values
            var index = 0;
            foreach (var element in queryPlayer.ToList())
            {
                sortedPlayerHand[index] = element;
                index++;
            }

            index = 0;
            foreach (var element in queryComputer.ToList())
            {
                sortedComputerHand[index] = element;
                index++;
            }
        }

        public void displayCards()
        {
            Console.Clear();
            int x = 0; // x axis position of the cursor. We can move it left & right
            int y = 1; // y axis position of the cursor. We can move it up & down

            // display player hand
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("PLAYER HAND");

            for (int i = 0; i < 5; i++)
            {
                DrawCards.drawCardOutline(x, y);
                DrawCards.drawCardSuitValue(sortedPlayerHand[i], x, y);
                x++;
            }

            y = 15; // move y cursor for computer hand below player hand
            x = 0; // reset x position

            // display computer hand            
            Console.SetCursorPosition(x, y - 1); // minus 1 to write line above drawn card
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine("COMPUTER HAND");

            for (int i = 5; i < 10; i++)
            {
                DrawCards.drawCardOutline(x, y);
                DrawCards.drawCardSuitValue(sortedComputerHand[i - 5], x, y);
                x++; // move drawCard x axis to the right otherwise all cards will be drawn in the same spot
            }
        }

        public void evaluateHand()
        {
            // create player's and cpu's evaluation object (passing SORTED HAND to evaluator)
            HandEvaluator playerHandEvaluator = new HandEvaluator(sortedPlayerHand);
            HandEvaluator cpuHandEvaluator = new HandEvaluator(sortedComputerHand);

            // get or return player's and computer's hand
            Hand playerHand = playerHandEvaluator.EvaluateHand();
            Hand computerHand = cpuHandEvaluator.EvaluateHand();

            //display each hand
            Console.WriteLine("\n\n\n\n\nPlayer Hand: " + playerHand);
            Console.WriteLine("\nComputer Hand: " + computerHand);

            // evaluate Hands
            if (playerHand > computerHand)
            {
                Console.WriteLine("Player WINS");
            }
            else if (playerHand < computerHand)
            {
                Console.WriteLine("Computer Wins");
            }
            else // if the hands are the same compare the value of the hands
            {
                // evaluate higher value of poker hand
                if (playerHandEvaluator.HandValues.Total > cpuHandEvaluator.HandValues.Total)
                {
                    Console.WriteLine("Player WINS");
                }
                else if (playerHandEvaluator.HandValues.Total < cpuHandEvaluator.HandValues.Total)
                {
                    Console.WriteLine("Computer WINS");
                }
                // if both have same poker hand, compare the highest card
                else if (playerHandEvaluator.HandValues.HighCard > cpuHandEvaluator.HandValues.HighCard)
                {
                    Console.WriteLine("Player WINS");
                }
                else if (playerHandEvaluator.HandValues.HighCard < cpuHandEvaluator.HandValues.HighCard)
                {
                    Console.WriteLine("Computer WINS");
                }
                else
                {
                    Console.WriteLine("DRAW");
                }
            }
        }
    }
}