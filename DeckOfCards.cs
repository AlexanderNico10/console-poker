﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Poker
{
    class DeckOfCards : Card // : means 'inherit' from parent class
    {
        const int num_of_cards = 52; // number of cards
        private Card[] deck; // array of all playing cards object

        public DeckOfCards()
        {
            deck = new Card[num_of_cards]; // constructs 52 new cards if 'num' is 52
        }

        public Card[] getDeck { get { return deck; } } // get current deck

        // Create deck of 52 cards: 13 values, with 4 suits, straight values
        public void setUpDeck()
        {
            int i = 0;

            foreach (SUIT s in Enum.GetValues(typeof(SUIT)))
            {
                foreach (CARDVALUE v in Enum.GetValues(typeof(CARDVALUE)))
                {
                    deck[i] = new Card { MySuit = s, MyValue = v };
                    i++;
                }
            }

            shuffleCards();
        }

        public void shuffleCards()
        {
            Random rand = new Random();
            Card temporaryCardValue;

            for (int shuffleTimes = 0; shuffleTimes < 10; shuffleTimes++)
            {
                for (int index = 0; index < num_of_cards; index++)
                {
                    // swop cards
                    int secondCardIndex = rand.Next(13); // 13 cards per suit
                    temporaryCardValue = deck[index];
                    deck[index] = deck[secondCardIndex];
                    deck[secondCardIndex] = temporaryCardValue;

                }
            }
        }
    }
}