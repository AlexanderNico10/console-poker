﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Poker
{
    public enum Hand
    {
        Nothing,
        OnePair,
        TwoPairs,
        ThreeKind,
        Straight,
        Flush,
        FullHouse,
        FourKind
    }

    public struct HandValue
    {
        public int Total { get; set; }
        public int HighCard { get; set; }
    }

    class HandEvaluator : Card
    {
        private int heartsSum;
        private int diamondsSum;
        private int clubsSum;
        private int spadesSum;

        private Card[] cardsDealt;
        private HandValue handValues;

        public HandEvaluator(Card[] sortedHand)
        {
            heartsSum = 0;
            diamondsSum = 0;
            clubsSum = 0;
            spadesSum = 0;
            cardsDealt = new Card[5];
            Cards = sortedHand;
            handValues = new HandValue();
        }

        public HandValue HandValues
        {
            get { return handValues; }
            set { handValues = value; }
        }

        public Card[] Cards
        {
            get { return cardsDealt; }
            set
            {
                cardsDealt[0] = value[0];
                cardsDealt[1] = value[1];
                cardsDealt[2] = value[2];
                cardsDealt[3] = value[3];
                cardsDealt[4] = value[4];
            }
        }

        public Hand EvaluateHand()
        {
            getNumberOfSuit();

            if (FourOfAKind())
            {
                return Hand.FourKind;
            }
            else if (FullHouse())
            {
                return Hand.FullHouse;
            }
            else if (Flush())
            {
                return Hand.Flush;
            }
            else if (Straight())
            {
                return Hand.Straight;
            }
            else if (ThreeOfAKind())
            {
                return Hand.ThreeKind;
            }
            else if (TwoPair())
            {
                return Hand.TwoPairs;
            }
            else if (OnePair())
            {
                return Hand.OnePair;
            }

            handValues.HighCard = (int)cardsDealt[4].MyValue;
            return Hand.Nothing;
        }

        private void getNumberOfSuit()
        {
            foreach (var element in Cards)
            {
                if (element.MySuit == Card.SUIT.HEARTS)
                {
                    heartsSum++;
                }
                else if (element.MySuit == SUIT.DIAMONDS)
                {
                    diamondsSum++;
                }
                else if (element.MySuit == SUIT.CLUBS)
                {
                    clubsSum++;
                }
                else if (element.MySuit == SUIT.SPADES)
                {
                    spadesSum++;
                }
            }
        }

        private bool FourOfAKind()
        {
            if (cardsDealt[0].MyValue == cardsDealt[1].MyValue && cardsDealt[0].MyValue == cardsDealt[2].MyValue && cardsDealt[0].MyValue == cardsDealt[3].MyValue)
            {
                handValues.Total = (int)cardsDealt[1].MyValue * 4; // eg. if four of a kind card is 9, then hand value is 9 * 4
                handValues.HighCard = (int)cardsDealt[4].MyValue; // determines higher card that isn't four
                return true;
            }
            else if (cardsDealt[1].MyValue == cardsDealt[2].MyValue && cardsDealt[1].MyValue == cardsDealt[3].MyValue && cardsDealt[1].MyValue == cardsDealt[4].MyValue)
            {
                handValues.Total = (int)cardsDealt[1].MyValue * 4;
                handValues.HighCard = (int)cardsDealt[0].MyValue;
                return true;
            }

            return false;
        }

        private bool FullHouse()
        {
            // first 3 cards and last 2 cards have same value eg. 3 Queens, 2 Aces
            if ((cardsDealt[0].MyValue == cardsDealt[1].MyValue && cardsDealt[0].MyValue == cardsDealt[2].MyValue && cardsDealt[3].MyValue == cardsDealt[4].MyValue) ||
                (cardsDealt[0].MyValue == cardsDealt[1].MyValue && cardsDealt[2].MyValue == cardsDealt[3].MyValue && cardsDealt[2].MyValue == cardsDealt[4].MyValue))
            {
                handValues.Total = (int)(cardsDealt[0].MyValue) + (int)(cardsDealt[1].MyValue) + (int)(cardsDealt[2].MyValue) +
                    (int)(cardsDealt[3].MyValue) + (int)(cardsDealt[4].MyValue);
                return true;
            }

            return false;
        }

        private bool Flush()
        {
            // the suit of all the cards is the same
            if (heartsSum == 5 || diamondsSum == 5 || spadesSum == 5 || clubsSum == 5)
            {
                handValues.Total = (int)cardsDealt[4].MyValue;
                return true;
            }

            return false;
        }

        private bool Straight() // checks whether a card is +1 to it's neighbour '10 is 9+1'
        {
            // 5 consecutive values eg. 9, 10, jack, queen, king
            if (cardsDealt[0].MyValue + 1 == cardsDealt[1].MyValue &&
                cardsDealt[1].MyValue + 1 == cardsDealt[2].MyValue &&
                cardsDealt[2].MyValue + 1 == cardsDealt[3].MyValue &&
                cardsDealt[3].MyValue + 1 == cardsDealt[4].MyValue)
            {
                handValues.Total = (int)cardsDealt[4].MyValue; // player with highest value of last card wins
            }

            return false;
        }

        private bool ThreeOfAKind()
        {
            if ((cardsDealt[0].MyValue == cardsDealt[1].MyValue && cardsDealt[0].MyValue == cardsDealt[2].MyValue) ||
                (cardsDealt[1].MyValue == cardsDealt[2].MyValue && cardsDealt[1].MyValue == cardsDealt[3].MyValue))
            {
                handValues.Total = (int)cardsDealt[2].MyValue * 3;
                handValues.HighCard = (int)cardsDealt[4].MyValue;
                return true;
            }
            else if (cardsDealt[2].MyValue == cardsDealt[3].MyValue && cardsDealt[2].MyValue == cardsDealt[4].MyValue)
            {
                handValues.Total = (int)cardsDealt[2].MyValue * 3;
                handValues.HighCard = (int)cardsDealt[4].MyValue;
                return true;
            }

            return false;
        }

        private bool TwoPair()
        {
            if (cardsDealt[0].MyValue == cardsDealt[1].MyValue && cardsDealt[2].MyValue == cardsDealt[3].MyValue)
            {
                handValues.Total = ((int)cardsDealt[1].MyValue * 2) + ((int)cardsDealt[2].MyValue * 2);
                handValues.HighCard = (int)cardsDealt[4].MyValue;
                return true;
            }
            else if (cardsDealt[0].MyValue == cardsDealt[1].MyValue && cardsDealt[3].MyValue == cardsDealt[4].MyValue)
            {
                handValues.Total = ((int)cardsDealt[1].MyValue * 2) + ((int)cardsDealt[3].MyValue * 2);
                handValues.HighCard = (int)cardsDealt[2].MyValue;
                return true;
            }
            else if (cardsDealt[1].MyValue == cardsDealt[2].MyValue && cardsDealt[3].MyValue == cardsDealt[4].MyValue)
            {
                handValues.Total = ((int)cardsDealt[1].MyValue * 2) + ((int)cardsDealt[3].MyValue * 2);
                handValues.HighCard = (int)cardsDealt[0].MyValue;
                return true;
            }

            return false; 
        }

        private bool OnePair()
        {
            if (cardsDealt[0].MyValue == cardsDealt[1].MyValue)
            {
                handValues.Total = (int)cardsDealt[0].MyValue * 2;
                handValues.HighCard = (int)cardsDealt[4].MyValue;
                return true;
            }
            else if (cardsDealt[1].MyValue == cardsDealt[2].MyValue)
            {
                handValues.Total = (int)cardsDealt[1].MyValue * 2;
                handValues.HighCard = (int)cardsDealt[4].MyValue;
                return true;
            }
            else if (cardsDealt[2].MyValue == cardsDealt[3].MyValue)
            {
                handValues.Total = (int)cardsDealt[2].MyValue * 2;
                handValues.HighCard = (int)cardsDealt[2].MyValue;
                return true;
            }
            else if (cardsDealt[3].MyValue == cardsDealt[4].MyValue)
            {
            }
            return false;
        }
    }
}
