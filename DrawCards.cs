﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Poker
{
    class DrawCards
    {
        public static void drawCardOutline(int xCoordinate, int yCoordinate)
        {
            Console.ForegroundColor = ConsoleColor.White;

            int x = xCoordinate * 12; // width of the card
            int y = yCoordinate;

            Console.SetCursorPosition(x, y); // starts drawing in the top corner of the x, y coordinate
            Console.Write(" __________\n"); // width of the card which will be the top edage of the card. These are 10 spaces but the width is actually 12

            for (int i = 0; i < 10; i++)
            {
                Console.SetCursorPosition(x, y + 1 + i);

                if (i != 9)
                {
                    Console.WriteLine("|          |"); // left and right edges of cards
                }
                else
                {
                    Console.WriteLine("|__________|");
                }
            }
        }

        public static void drawCardSuitValue(Card card, int xCoor, int yCoor) // displays suit and value of card
        {
            char cardSuit = ' ';
            int x = xCoor * 12;
            int y = yCoor;

            // Encode each byte array from the CodePage437 into a character
            // Hearts & Diamonds are red, Clubs & Spades are black

            switch (card.MySuit)
            {
                case Card.SUIT.HEARTS:
                    cardSuit = Encoding.GetEncoding(437).GetChars(new byte[] {3}) [0];
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case Card.SUIT.DIAMONDS:
                    cardSuit = Encoding.GetEncoding(437).GetChars(new byte[] { 4 })[0];
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case Card.SUIT.CLUBS:
                    cardSuit = Encoding.GetEncoding(437).GetChars(new byte[] { 5 })[0];
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
                case Card.SUIT.SPADES:
                    cardSuit = Encoding.GetEncoding(437).GetChars(new byte[] { 6 })[0];
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
            }

            Console.SetCursorPosition(x + 5, y + 5);
            Console.Write(cardSuit);
            Console.SetCursorPosition(x + 4, y + 7);
            Console.Write(card.MyValue);
        }
    }
}
