Console Poker
===

Licensing Information: READ LICENSE
---

Project source: https://AlexanderNico10@bitbucket.org/AlexanderNico10/console-poker.git
---

File List
---

```
Card.cs
DealCards.cs
DeckOfCards.cs
DrawCards.cs
HandEvaluator.cs
Poker.csproj
Program.cs
LICENSE
README.md
```

How to run app
---

Import the application into Visual Studio and press CTRL + F5. Further features and draw
types to be added once I've constructed an Android port.